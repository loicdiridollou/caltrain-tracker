import datetime
import requests
import pandas as pd


def get_data(stop_name=None):
    url = "https://www.caltrain.com/gtfs/stops/{}/predictions".format(stop_name)
    source = requests.get(url).json()
    return source


def flatten_dictionary(dic):
    res = {}
    for el in dic:
        if isinstance(dic[el], dict):
            tmp = flatten_dictionary(dic[el])
            for ell in tmp:
                res[str(el) + "." + str(ell)] = tmp[ell]
        else:
            res[el] = dic[el]
    return res


def transform_data(source, direction=1):
    list_preds = source['data'][direction]['predictions']
    tab = []
    for el in list_preds:
        res = flatten_dictionary(el['TripUpdate']['StopTimeUpdate'][0])
        res['train_number'] = el['Id']
        res['Route'] = el['TripUpdate']['Trip']['RouteId']
        tab.append(res)
    df = pd.DataFrame(tab)
    if 'Arrival.Time' in df:
        df['Arrival.Time'] = df['Arrival.Time'].apply(datetime.datetime.fromtimestamp)
    df['Departure.Time'] = df['Departure.Time'].apply(datetime.datetime.fromtimestamp)
    df = df.sort_values('Departure.Time')
    df.reset_index(drop=True, inplace=True)
    return df


if __name__ == "__main__":
    source = get_data('22nd_street')
    print(transform_data(source, 1))

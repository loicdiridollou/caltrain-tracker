"""Module containing different parameters and mappings."""


def caltrain_stations():
    stations = {"san_francisco": "San Francisco Caltrain Station"}
    return stations


def train_directions():
    return {"0": "Northbound",
            "1": "Southbound"}

dic = {'StopId': '70012', 'Departure': {'Time': 1652920740}}

def flatten_dictionary(dic):
    res = {}
    for el in dic:
        if isinstance(dic[el], dict):
            tmp = flatten_dictionary(dic[el])
            for ell in tmp:
                res[str(el) + "." + str(ell)] = tmp[ell]
        else:
            res[el] = dic[el]
    return res

